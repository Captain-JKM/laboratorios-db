import CRUD.CloudConnectionPostgre;
import CRUD.Delete;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) {

        DataSource dataSource = CloudConnectionPostgre.createConnection();

        try(Connection connection = dataSource.getConnection()){

            String studentIdToDelete = "1"; 
            Delete.deleteStudentById(studentIdToDelete, connection);

        }catch (SQLException e){
            e.printStackTrace();
            System.out.println("Error obtener: " + e.getMessage());
        }

        /*try (Connection connection = dataSource.getConnection()) {
            CRUD.Student student = new CRUD.Student();
            student.setIdStudent("1");
            student.setNombreStudent("Edgar");
            student.setApellidoStudent("Rodriguez");
            student.setCursoStudent("Ciencias");
            student.setSeguroStudent("Yes");

            CRUD.Create.createStudent(student, connection);
            System.out.println("Se ha creado correctamente");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Error de CRUD.Student es: " + e.getMessage());
        }


         */
    }
}