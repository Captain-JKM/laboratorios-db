package CRUD;

import com.zaxxer.hikari.HikariConfig;

import javax.sql.DataSource;

import com.zaxxer.hikari.HikariDataSource;

public class CloudConnectionPostgre {

    private static final String INSTANCE_CONNECTION_NAME =
            "centered-kiln-418416:us-central1:postgre-sql-ts";
    private static final String DB_USER = "postgres";
    private static final String DB_PASS = "\"ATi,e=t]@[3k1}$";
    private static final String DB_NAME = "base-captain-ts4";

    public static DataSource createConnection() {

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(String.format("jdbc:postgresql:///%s", DB_NAME));
        config.setUsername(DB_USER);
        config.setPassword(DB_PASS);
        config.addDataSourceProperty("socketFactory", "com.google.cloud.sql.postgres.SocketFactory");
        config.addDataSourceProperty("cloudSqlInstance", INSTANCE_CONNECTION_NAME);
        return new HikariDataSource(config);
    }

}
