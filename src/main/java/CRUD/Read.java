package CRUD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Read {

    public static Student getStudentById(String id, Connection connection) {
        String sql = "SELECT * FROM Students WHERE idStudent = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Student student = new Student();
                    student.setIdStudent(resultSet.getString("idStudent"));
                    student.setNombreStudent(resultSet.getString("nombreStudent"));
                    student.setApellidoStudent(resultSet.getString("apellidoStudent"));
                    student.setCursoStudent(resultSet.getString("cursoStudent"));
                    student.setSeguroStudent(resultSet.getString("seguroStudent"));
                    return student;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}