package CRUD;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Update {

    public static void updateStudent(Student student, Connection connection) {
        String sql = "UPDATE Students SET nombreStudent = ?, apellidoStudent = ?, cursoStudent = ?, seguroStudent = ? WHERE idStudent = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, student.getNombreStudent());
            statement.setString(2, student.getApellidoStudent());
            statement.setString(3, student.getCursoStudent());
            statement.setString(4, student.getSeguroStudent());
            statement.setString(5, student.getIdStudent());
            statement.executeUpdate();
            System.out.println("Estudiante actualizado exitosamente.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
