package CRUD;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Create {

    public static void createStudent(Student student, Connection connection) {
        String sql = "INSERT INTO Students (idStudent, nombreStudent, apellidoStudent, cursoStudent, seguroStudent) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, student.getIdStudent());
            statement.setString(2, student.getNombreStudent());
            statement.setString(3, student.getApellidoStudent()); // Nuevo campo
            statement.setString(4, student.getCursoStudent());
            statement.setString(5, student.getSeguroStudent());
            statement.executeUpdate();
            System.out.println("Estudiante creado exitosamente.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
