package CRUD;

public class Student {
    private String idStudent;
    private String nombreStudent;
    private String apellidoStudent;
    private String cursoStudent;
    private String seguroStudent;

    public String getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(String idStudent) {
        this.idStudent = idStudent;
    }

    public String getNombreStudent() {
        return nombreStudent;
    }

    public void setNombreStudent(String nombreStudent) {
        this.nombreStudent = nombreStudent;
    }

    public String getCursoStudent() {
        return cursoStudent;
    }

    public void setCursoStudent(String cursoStudent) {
        this.cursoStudent = cursoStudent;
    }

    public String getSeguroStudent() {
        return seguroStudent;
    }

    public void setSeguroStudent(String seguroStudent) {
        this.seguroStudent = seguroStudent;
    }

    public String getApellidoStudent() {
        return apellidoStudent;
    }

    public void setApellidoStudent(String apellidoStudent) {
        this.apellidoStudent = apellidoStudent;
    }
}
