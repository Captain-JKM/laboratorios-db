package CRUD;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Delete {

    public static void deleteStudentById(String id, Connection connection) {
        String sql = "DELETE FROM Students WHERE idStudent = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
            System.out.println("Estudiante eliminado exitosamente.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
